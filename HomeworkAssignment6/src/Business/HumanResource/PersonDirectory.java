/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HumanResource;

import java.util.ArrayList;

/**
 *
 * @author kanda
 */
public class PersonDirectory {
    
    private ArrayList<Person> personDirectoryList;
    
    public PersonDirectory()
    {
        personDirectoryList = new  ArrayList<Person>();
        
    }

    public ArrayList<Person> getPersonDirectoryList() {
        return personDirectoryList;
    }
    
    public Person addPersonToDirectory()
    {
        Person obj = new Person();
        personDirectoryList.add(obj);
        return obj;
    }
    
    public Person findPersonByFirstName(String firstName)
    {
        for(Person p : personDirectoryList)
        {
            if(p.getFirstName().equals(firstName))
            {
                return p;
            }
        }
        return null;
    }
    
    public void removePerson(Person p)
    {
        personDirectoryList.remove(p);
    }

}
