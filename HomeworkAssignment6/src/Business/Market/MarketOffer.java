/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Market;

import Business.Product.Product;
import java.util.HashMap;

/**
 *
 * @author kanda
 */
public class MarketOffer {
    
    private HashMap<Market , Product> marketOffer;
    
    public MarketOffer()
    {
        marketOffer = new HashMap<Market , Product>();
    }

    public HashMap<Market, Product> getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(HashMap<Market, Product> marketOffer) {
        this.marketOffer = marketOffer;
    }
    
}
