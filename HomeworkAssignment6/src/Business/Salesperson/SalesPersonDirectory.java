
package Business.Salesperson;


import java.util.ArrayList;

/**
 *
 * @author Gauri Y
 */
public class SalesPersonDirectory {
 
    private ArrayList<SalesPerson> salespersonList;
    
    public SalesPersonDirectory() 
    {
        salespersonList = new ArrayList<SalesPerson>();
    }

    public ArrayList<SalesPerson> getSalespersonList() {
        return salespersonList;
    }

     public SalesPerson addSalesPersonToDirectory()
    {
        SalesPerson salesperson = new SalesPerson();
        salespersonList.add(salesperson);
        return salesperson;
    }
    
    public SalesPerson findSalesPersonByName(String salespersonName)
    {
        for(SalesPerson s : salespersonList)
        {
            if(s.getSalespersonName().equals(salespersonName))
            {
                return s;
            }
        }
        return null;
    }
    
    
        public SalesPerson searchSalesPerson(String sp){
        for (SalesPerson salesperson : salespersonList) {
            if(salesperson.getSalespersonName().equals(sp)){
                return salesperson;
            }
        }
        return null;
    }

public SalesPerson isValidUser(String spId , String sppassword)
   {
       for(SalesPerson sp : salespersonList)
       {
           if(sp.getSalesUserId().equals(spId) && sp.getSalesPassword().equals(sppassword))
           {
               return sp;
           }
       }
       return null;
   }
     
    
    public void removeSalesPerson(SalesPerson s)
    {
        salespersonList.remove(s);
    }
    
}
