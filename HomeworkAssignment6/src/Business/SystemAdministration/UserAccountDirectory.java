/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SystemAdministration;

import Business.HumanResource.Person;
import java.util.ArrayList;

/**
 *
 * @author kanda
 */
public class UserAccountDirectory {
    
    private ArrayList<UserAccount> userAccountList;
    
    public UserAccountDirectory()
    {
        userAccountList = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

   public UserAccount addUserAccountToDirectory()
   {
       UserAccount obj = new UserAccount();
       userAccountList.add(obj);
       return obj;
   }
   
   public void deleteUserAccount(UserAccount ua)
   {
       userAccountList.remove(ua);
   }
   
   public void activeInactiveUserAccount(UserAccount ua)
   {
        for(UserAccount obj : userAccountList)
        {
            if(obj.getUserId().equals(ua.getUserId()))
            {
                if(ua.getAccountStatus().equals("Active"))
                {
                    obj.setAccountStatus("Inactive");
                    break;
                }
                else
                {
                    obj.setAccountStatus("Active");
                    break;
                }
            }
        }
   }
   
   public UserAccount isValidUser(String userId , String password)
   {
       for(UserAccount obj : userAccountList)
       {
           if(obj.getUserId().equals(userId) && obj.getPassword().equals(password))
           {
               return obj;
           }
       }
       return null;
   }
   
   public ArrayList<UserAccount> findUserAccount(Person person)
   {
       ArrayList<UserAccount> tmp = new ArrayList<UserAccount>();
       
       for(UserAccount obj : userAccountList)
       {
           if(obj.getPerson().toString().equals(person.toString()))
           {
               tmp.add(obj);
           }
       }
       return tmp;
   }
   
}
