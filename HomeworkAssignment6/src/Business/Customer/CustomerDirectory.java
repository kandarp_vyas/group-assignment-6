/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author kanda
 */
public class CustomerDirectory {
    
    private ArrayList<Customer> customerList;
    
    public CustomerDirectory()
    {
        customerList = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public Customer addCustomerToDirectory()
    {
        Customer cus = new Customer();
        customerList.add(cus);
        return cus;
    }
    
    public Customer findCustomerByFirstName(String firstName)
    {
        for(Customer c : customerList)
        {
            if(c.getFirstName().equals(firstName))
            {
                return c;
            }
        }
        return null;
    }
    
    public void removeCustomer(Customer c)
    {
        customerList.remove(c);
    }
    
    
    
}
