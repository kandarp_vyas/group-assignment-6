/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Order;

import java.util.ArrayList;

/**
 *
 * @author kanda
 */
public class OrderDirectory {
    
    private ArrayList<Order> orderList;
    
    public  OrderDirectory()
    {
        orderList = new ArrayList<Order>();
    }

    public Order addOrder(Order o)
    {
        orderList.add(o);
        return o;
        
    }
    public void removeOrder(Order o)
    {
        orderList.remove(o); 
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }
    
}
