/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Order;

import Business.Customer.Customer;
import Business.Product.Product;
import java.util.ArrayList;

/**
 *
 * @author kanda
 */
public class Order {
    
    private ArrayList<OrderItem> orderItemList;
    
    private int orderNumber;
    
    private static int count = 0;
    
    private Customer customer;
    
    public Order()
    {
        count++;
        orderNumber = count;
        orderItemList = new ArrayList<OrderItem>();
        customer = new Customer();
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
    public OrderItem addOrderItem(Product p, int q, double price)
    {
        OrderItem o = new OrderItem();
        o.setProduct(p);
        o.setQuantity(q);
        o.setSalesPrice(price);
        
        orderItemList.add(o);
        return o;
    }
    public void removeOrderItem(OrderItem o)
    {
        orderItemList.remove(o); 
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
}
