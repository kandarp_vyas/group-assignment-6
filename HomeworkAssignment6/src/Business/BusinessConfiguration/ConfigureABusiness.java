/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BusinessConfiguration;

import Business.Business;
import Business.HumanResource.Person;
import Business.HumanResource.PersonDirectory;
import Business.SystemAdministration.UserAccount;
import Business.SystemAdministration.UserAccountDirectory;

/**
 *
 * @author kanda
 */
public class ConfigureABusiness {
    
    public static Business Initialize(String name)
    {
        Business businessObj = new Business(name);
        
        PersonDirectory personDirectory = businessObj.getPersonDirectory();
        
        Person p1 = personDirectory.addPersonToDirectory();
        p1.setFirstName("kandarp");
        p1.setLastName("vyas");
        //p1.setGender("Male");
        p1.setSSN("123");
        
        Person p2 = personDirectory.addPersonToDirectory();
        p2.setFirstName("hardik");
        p2.setLastName("padhiar");
        //p2.setGender("Male");
        p2.setSSN("456");
        
        Person p3 = personDirectory.addPersonToDirectory();
        p3.setFirstName("vinay");
        p3.setLastName("gandhi");
        //p3.setGender("Male");
        p3.setSSN("9898989");
        
        UserAccountDirectory uad = businessObj.getUserAccountDirectory();
        
        Person tmp = personDirectory.findPersonByFirstName("kandarp");
        
        if(tmp!=null)
        {
            UserAccount ua1 = uad.addUserAccountToDirectory();
            ua1.setPerson(tmp);
            ua1.setUserId("kandarp.admin");
            ua1.setAccountStatus("Active");
            ua1.setRole("System Admin");
            ua1.setPassword("abcd");
            
            UserAccount ua2 = uad.addUserAccountToDirectory();
            ua2.setPerson(tmp);
            ua2.setUserId("kandarp.HR");
            ua2.setAccountStatus("Active");
            ua2.setRole("HR");
            ua2.setPassword("tcs");
            
        }
        
        Person tmpVinay = personDirectory.findPersonByFirstName("vinay");
        
        if(tmpVinay!=null)
        {
            UserAccount ua1 = uad.addUserAccountToDirectory();
            ua1.setPerson(tmpVinay);
            ua1.setUserId("vinay.employee");
            ua1.setAccountStatus("Active");
            ua1.setRole("Employee");
            ua1.setPassword("qwer");
        }
        
        return businessObj;
    }
    
}
