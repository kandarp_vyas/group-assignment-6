/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Customer.CustomerDirectory;
import Business.HumanResource.PersonDirectory;
import Business.Order.Order;
import Business.Order.OrderDirectory;
import Business.Salesperson.SalesPerson;
import Business.Salesperson.SalesPersonDirectory;
import Business.Supplier.SupplierDirectory;
import Business.SystemAdministration.UserAccountDirectory;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author kanda
 */
public class Business {
    
    private String name;
    
    private PersonDirectory personDirectory;
    
    private UserAccountDirectory userAccountDirectory;
    
    private SupplierDirectory supplierDirectory;
    
    private CustomerDirectory customerDirectory;
    
    private OrderDirectory orderDirectory;
    
    private SalesPersonDirectory salesPersonDirectory;
    
    private HashMap<SalesPerson ,ArrayList<Order>> salesPersonOrderList;

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }
    
    
    public Business(String name)
    {
        this.name = name;
        
        personDirectory = new PersonDirectory();
        
        userAccountDirectory = new UserAccountDirectory();
        
        supplierDirectory = new SupplierDirectory();
        
        customerDirectory = new CustomerDirectory();
        
        orderDirectory = new OrderDirectory();
        
        salesPersonDirectory = new SalesPersonDirectory();
        
        salesPersonOrderList = new HashMap<SalesPerson ,ArrayList<Order>>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public OrderDirectory getOrderDirectory() {
        return orderDirectory;
    }

    public void setOrderDirectory(OrderDirectory orderDirectory) {
        this.orderDirectory = orderDirectory;
    }

    public HashMap<SalesPerson, ArrayList<Order>> getSalesPersonOrderList() {
        return salesPersonOrderList;
    }

    public void setSalesPersonOrderList(HashMap<SalesPerson, ArrayList<Order>> salesPersonOrderList) {
        this.salesPersonOrderList = salesPersonOrderList;
    }
    
}
