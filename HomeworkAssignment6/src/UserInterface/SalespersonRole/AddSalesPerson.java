/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SalespersonRole;

import Business.Business;
import Business.Salesperson.SalesPerson;
import Business.SystemAdministration.UserAccount;
import UserInterface.AdministrativeRole.ManageSuppliers;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gauri Y
 */
public class AddSalesPerson extends javax.swing.JPanel {

    /**
     * Creates new form AddSalesPerson
     */
    private Business business;
    private JPanel userProcessContainer;
    private UserAccount ua;


    AddSalesPerson(JPanel userProcessContainer, Business business, UserAccount ua) {
        initComponents();
        this.business=business;
        this.userProcessContainer=userProcessContainer;
        this.ua=ua;
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtsalespersonname = new javax.swing.JTextField();
        txtsalespersonid = new javax.swing.JTextField();
        btnbacksalesperson = new javax.swing.JButton();
        btncreatesalesperson = new javax.swing.JButton();
        txtsalespersonpassword = new javax.swing.JPasswordField();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("ADD SALESPERSON");

        jLabel2.setText("Name:");

        jLabel3.setText("Uer ID:");

        jLabel4.setText("Password:");

        btnbacksalesperson.setText("<< BACK");
        btnbacksalesperson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbacksalespersonActionPerformed(evt);
            }
        });

        btncreatesalesperson.setText("CREATE");
        btncreatesalesperson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncreatesalespersonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(285, 285, 285)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(222, 222, 222)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(59, 59, 59)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtsalespersonname)
                            .addComponent(txtsalespersonid)
                            .addComponent(txtsalespersonpassword, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(180, 180, 180)
                        .addComponent(btnbacksalesperson)
                        .addGap(130, 130, 130)
                        .addComponent(btncreatesalesperson)))
                .addContainerGap(198, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtsalespersonname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtsalespersonid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtsalespersonpassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(92, 92, 92)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnbacksalesperson)
                    .addComponent(btncreatesalesperson))
                .addContainerGap(328, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btncreatesalespersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncreatesalespersonActionPerformed

         String salespersonName=txtsalespersonname.getText();
        String salespersonId=txtsalespersonid.getText();
        String salespersonPassword=txtsalespersonpassword.getText();
        
        if(salespersonName.equals(""))
        {
            JOptionPane.showMessageDialog(null, "Salespersons name cannot be blank.");
        }
        else if(salespersonId.equals(""))
         {
            JOptionPane.showMessageDialog(null, "Salesperson ID cannot be blank.");
         }
         
         else if(salespersonPassword.equals(""))
         {
            JOptionPane.showMessageDialog(null, "Password cannot be blank.");
         }    
        else
        {
             SalesPerson sp = business.getSalesPersonDirectory().addSalesPersonToDirectory();
             sp.setSalespersonName(salespersonName);
             sp.setSalesUserId(salespersonId);
             sp.setSalesPassword(salespersonPassword);
             
            JOptionPane.showMessageDialog(null, "Added successfully!!", "Info", JOptionPane.INFORMATION_MESSAGE);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_btncreatesalespersonActionPerformed

    private void btnbacksalespersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbacksalespersonActionPerformed

        userProcessContainer.remove(this);
       Component [] componentArray = userProcessContainer.getComponents();
        Component c = componentArray[componentArray.length-1];
        ManageSalesPerson msp = (ManageSalesPerson) c;
        msp.populateTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    
    }//GEN-LAST:event_btnbacksalespersonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnbacksalesperson;
    private javax.swing.JButton btncreatesalesperson;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtsalespersonid;
    private javax.swing.JTextField txtsalespersonname;
    private javax.swing.JPasswordField txtsalespersonpassword;
    // End of variables declaration//GEN-END:variables
}
