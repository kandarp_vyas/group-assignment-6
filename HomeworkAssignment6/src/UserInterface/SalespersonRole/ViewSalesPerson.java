/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SalespersonRole;

import Business.Business;
import Business.Salesperson.SalesPerson;
import UserInterface.AdministrativeRole.ManageSuppliers;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gauri Y
 */
public class ViewSalesPerson extends javax.swing.JPanel {

   private Business business;
   private JPanel userProcessContainer;
   private SalesPerson sp;

    ViewSalesPerson(JPanel userProcessContainer, SalesPerson sp) {
        initComponents();
        this.business=business;
        this.userProcessContainer=userProcessContainer;
        this.sp=sp;
        populateTable();
        txtsalespersonname.setEnabled(false);
        txtsalesuserid.setEnabled(false);
        txtsalespassword.setEnabled(false);
        btnsavesales.setEnabled(false);
        btnupdatesales.setEnabled(true);
    }


    public void populateTable()
    {
      txtsalespersonname.setText(sp.getSalespersonName());
        txtsalesuserid.setText(sp.getSalesUserId());
        txtsalespassword.setText(sp.getSalesPassword());
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtsalespersonname = new javax.swing.JTextField();
        txtsalesuserid = new javax.swing.JTextField();
        txtsalespassword = new javax.swing.JTextField();
        btnsalesback = new javax.swing.JButton();
        btnsavesales = new javax.swing.JButton();
        btnupdatesales = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("VIEW SALESPERSON");

        jLabel2.setText("Name:");

        jLabel3.setText("User ID:");

        jLabel4.setText("Password:");

        txtsalespersonname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsalespersonnameActionPerformed(evt);
            }
        });

        btnsalesback.setText("<< BACK");
        btnsalesback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalesbackActionPerformed(evt);
            }
        });

        btnsavesales.setText("SAVE");
        btnsavesales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsavesalesActionPerformed(evt);
            }
        });

        btnupdatesales.setText("UPDATE");
        btnupdatesales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnupdatesalesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(181, 181, 181)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(96, 96, 96)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtsalespersonname)
                            .addComponent(txtsalesuserid)
                            .addComponent(txtsalespassword, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(btnsalesback, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(88, 88, 88)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(btnsavesales, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(108, 108, 108)
                                .addComponent(btnupdatesales, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1))))
                .addContainerGap(144, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtsalespersonname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtsalesuserid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtsalespassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsalesback, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsavesales, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnupdatesales, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(336, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtsalespersonnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsalespersonnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtsalespersonnameActionPerformed

    private void btnsavesalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsavesalesActionPerformed

        sp.setSalespersonName(txtsalespersonname.getText());
        sp.setSalesUserId(txtsalesuserid.getText());      
        sp.setSalesPassword(txtsalespassword.getText());
        
        
        btnsavesales.setEnabled(false);
        btnupdatesales.setEnabled(true);
        JOptionPane.showMessageDialog(null, "SalesPerson details updated successfully");
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsavesalesActionPerformed

    private void btnupdatesalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnupdatesalesActionPerformed

        txtsalespersonname.setEnabled(false);
        txtsalesuserid.setEnabled(true);
        txtsalespassword.setEnabled(true);
       
        btnsavesales.setEnabled(true);
        btnupdatesales.setEnabled(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_btnupdatesalesActionPerformed

    private void btnsalesbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalesbackActionPerformed

         userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component  = componentArray[componentArray.length-1];
        ManageSalesPerson managesalesperson = (ManageSalesPerson) component;
        managesalesperson.populateTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        // TODO add your handling code here:
    }//GEN-LAST:event_btnsalesbackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnsalesback;
    private javax.swing.JButton btnsavesales;
    private javax.swing.JButton btnupdatesales;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtsalespassword;
    private javax.swing.JTextField txtsalespersonname;
    private javax.swing.JTextField txtsalesuserid;
    // End of variables declaration//GEN-END:variables
}
