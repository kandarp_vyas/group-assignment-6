/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdministrativeRole;

import Business.Business;
import Business.Supplier.Supplier;
import Business.SystemAdministration.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kanda
 */
public class ManageSuppliers extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
     
    private Business business;
     
    private UserAccount ua;
    /**
     * Creates new form ManageSuppliers
     */
    public ManageSuppliers(JPanel userProcessContainer , Business business , UserAccount ua) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.ua = ua;
        refreshTable();
    }
    
     public void refreshTable(){
        int rowCount = tblManageSupplierTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) tblManageSupplierTable.getModel();
        for(int i=rowCount-1;i>=0;i--){
            model.removeRow(i);
        }
        for (Supplier s : business.getSupplierDirectory().getSupplierlist()) {
            Object row[] = new Object[2];
            row[0] = s;
            row[1]=s.getSupplierID();
            model.addRow(row);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblManageSupplierPage = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblManageSupplierTable = new javax.swing.JTable();
        btnAddSupplier = new javax.swing.JButton();
        btnviewsuppliercatalog = new javax.swing.JButton();
        btnDeleteSupplier = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnviewsupplier = new javax.swing.JButton();

        lblManageSupplierPage.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        lblManageSupplierPage.setText("Manage Supplier Page");

        tblManageSupplierTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Supplier Name", "Username"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblManageSupplierTable);
        if (tblManageSupplierTable.getColumnModel().getColumnCount() > 0) {
            tblManageSupplierTable.getColumnModel().getColumn(0).setResizable(false);
            tblManageSupplierTable.getColumnModel().getColumn(1).setResizable(false);
        }

        btnAddSupplier.setText("Add Supplier");
        btnAddSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSupplierActionPerformed(evt);
            }
        });

        btnviewsuppliercatalog.setText("View Supplier Catalog");
        btnviewsuppliercatalog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewsuppliercatalogActionPerformed(evt);
            }
        });

        btnDeleteSupplier.setText("Delete Supplier");
        btnDeleteSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteSupplierActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnviewsupplier.setText("View/Update Supplier");
        btnviewsupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewsupplierActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(408, 408, 408)
                        .addComponent(lblManageSupplierPage, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 567, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnAddSupplier)
                                .addGap(367, 367, 367)
                                .addComponent(btnDeleteSupplier))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(396, 396, 396)
                        .addComponent(btnviewsupplier))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(346, 346, 346)
                        .addComponent(btnviewsuppliercatalog, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(434, 434, 434)
                        .addComponent(btnBack)))
                .addContainerGap(330, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(lblManageSupplierPage)
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddSupplier)
                    .addComponent(btnDeleteSupplier)
                    .addComponent(btnviewsupplier))
                .addGap(43, 43, 43)
                .addComponent(btnviewsuppliercatalog, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(btnBack)
                .addContainerGap(263, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSupplierActionPerformed
        // TODO add your handling code here:
        AddSupplier as = new AddSupplier(userProcessContainer, business , ua);
        userProcessContainer.add("AddSupplier", as);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnAddSupplierActionPerformed

    private void btnDeleteSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteSupplierActionPerformed
        // TODO add your handling code here:
        int row = tblManageSupplierTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Please select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Supplier s = (Supplier) tblManageSupplierTable.getValueAt(row, 0);
        business.getSupplierDirectory().removeSupplier(s);
        refreshTable();
    }//GEN-LAST:event_btnDeleteSupplierActionPerformed

    private void btnviewsuppliercatalogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewsuppliercatalogActionPerformed
        // TODO add your handling code here:
        int row = tblManageSupplierTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Please select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Supplier s = (Supplier)tblManageSupplierTable.getValueAt(row,0);
        ViewSupplierCatalog vsc = new ViewSupplierCatalog(userProcessContainer, s);
        userProcessContainer.add("ViewSupplier", vsc);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnviewsuppliercatalogActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnviewsupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewsupplierActionPerformed

        int row = tblManageSupplierTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Please select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else{
        Supplier s = (Supplier)tblManageSupplierTable.getValueAt(row,0);
        ViewSupplier vs = new ViewSupplier(userProcessContainer, s);
        userProcessContainer.add("ViewSupplier", vs);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        }
       
                


        // TODO add your handling code here:
    }//GEN-LAST:event_btnviewsupplierActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddSupplier;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDeleteSupplier;
    private javax.swing.JButton btnviewsupplier;
    private javax.swing.JButton btnviewsuppliercatalog;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblManageSupplierPage;
    private javax.swing.JTable tblManageSupplierTable;
    // End of variables declaration//GEN-END:variables
}
