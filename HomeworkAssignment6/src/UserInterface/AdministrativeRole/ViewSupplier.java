/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdministrativeRole;

import Business.Supplier.Supplier;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gauri Y
 */
public class ViewSupplier extends javax.swing.JPanel {

   
private JPanel userProcessContainer;
private Supplier s;

    ViewSupplier(JPanel userProcessContainer, Supplier s) {
      initComponents();
      this.userProcessContainer=userProcessContainer;
      this.s=s;
      refreshTable();
        txtsuppliername.setEnabled(false);
        txtsupplierid.setEnabled(false);
        txtsupplierpassword.setEnabled(false);
        btnsavesupplier.setEnabled(false);
        btnupdatesupplier.setEnabled(true);
    }
    
       public void refreshTable(){
        txtsuppliername.setText(s.getSupplyName());
        txtsupplierid.setText(s.getSupplierID());
        txtsupplierpassword.setText(s.getSupplierPassword());
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtsuppliername = new javax.swing.JTextField();
        txtsupplierid = new javax.swing.JTextField();
        txtsupplierpassword = new javax.swing.JTextField();
        btnbackviewsupplier = new javax.swing.JButton();
        btnsavesupplier = new javax.swing.JButton();
        btnupdatesupplier = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("VIEW SUPPLIER DETAILS");

        jLabel2.setText("Supplier Name:");

        jLabel3.setText("User ID:");

        jLabel4.setText("Password:");

        btnbackviewsupplier.setText("BACK");
        btnbackviewsupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackviewsupplierActionPerformed(evt);
            }
        });

        btnsavesupplier.setText("SAVE");
        btnsavesupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsavesupplierActionPerformed(evt);
            }
        });

        btnupdatesupplier.setText("UPDATE");
        btnupdatesupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnupdatesupplierActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(182, 182, 182))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(135, 135, 135)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(68, 68, 68)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtsuppliername)
                            .addComponent(txtsupplierid)
                            .addComponent(txtsupplierpassword, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(btnbackviewsupplier)
                        .addGap(100, 100, 100)
                        .addComponent(btnsavesupplier)
                        .addGap(87, 87, 87)
                        .addComponent(btnupdatesupplier)))
                .addContainerGap(142, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtsuppliername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtsupplierid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtsupplierpassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnbackviewsupplier)
                    .addComponent(btnsavesupplier)
                    .addComponent(btnupdatesupplier))
                .addContainerGap(160, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnsavesupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsavesupplierActionPerformed

        s.setSupplyName(txtsuppliername.getText());
        s.setSupplierID(txtsupplierid.getText());      
        s.setSupplierPassword(txtsupplierpassword.getText());
        
        
        btnsavesupplier.setEnabled(false);
        btnupdatesupplier.setEnabled(true);
        JOptionPane.showMessageDialog(null, "Supplier details updated successfully");

    }//GEN-LAST:event_btnsavesupplierActionPerformed

    private void btnupdatesupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnupdatesupplierActionPerformed

        txtsuppliername.setEnabled(false);
        txtsupplierid.setEnabled(true);
        txtsupplierpassword.setEnabled(true);
       
        btnsavesupplier.setEnabled(true);
        btnupdatesupplier.setEnabled(false);

    }//GEN-LAST:event_btnupdatesupplierActionPerformed

    private void btnbackviewsupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackviewsupplierActionPerformed
        
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component  = componentArray[componentArray.length-1];
        ManageSuppliers managesuppliers = (ManageSuppliers) component;
        managesuppliers.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
        
    }//GEN-LAST:event_btnbackviewsupplierActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnbackviewsupplier;
    private javax.swing.JButton btnsavesupplier;
    private javax.swing.JButton btnupdatesupplier;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtsupplierid;
    private javax.swing.JTextField txtsuppliername;
    private javax.swing.JTextField txtsupplierpassword;
    // End of variables declaration//GEN-END:variables
}
