/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerRole;

import Business.Business;
import Business.Customer.Customer;
import Business.SystemAdministration.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gauri Y
 */
public class ManageCustomerJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageCustomerJPanel
     */
    private Business business;
    private JPanel userProcessContainer;
    private UserAccount ua;
    
    public ManageCustomerJPanel(JPanel userProcessContainer , Business business , UserAccount ua) {
        initComponents();
        this.business=business;
        this.userProcessContainer=userProcessContainer;
        this.ua=ua;
        populateCustomerDetails();
        
    }

    
    @SuppressWarnings("unchecked")
    
     public void populateCustomerDetails(){
        int rowCount = jtablemanagecustomers.getRowCount();
        DefaultTableModel model = (DefaultTableModel) jtablemanagecustomers.getModel();
        for(int i=rowCount-1;i>=0;i--){
            model.removeRow(i);
        }
        for (Customer c: business.getCustomerDirectory().getCustomerList()) {
            Object row[] = new Object[2];
            row[0] = c.getCustomerName();
            row[1]=c.getSSN();
        
            model.addRow(row);
        }
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtablemanagecustomers = new javax.swing.JTable();
        btnbackcustomer = new javax.swing.JButton();
        btnaddcustomer = new javax.swing.JButton();
        btnviewcustomer = new javax.swing.JButton();
        btndeletecustomer = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("MANAGE CUSTOMERS");

        jtablemanagecustomers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "SSN"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jtablemanagecustomers);
        if (jtablemanagecustomers.getColumnModel().getColumnCount() > 0) {
            jtablemanagecustomers.getColumnModel().getColumn(0).setResizable(false);
            jtablemanagecustomers.getColumnModel().getColumn(1).setResizable(false);
        }

        btnbackcustomer.setText("<< Back");
        btnbackcustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbackcustomerActionPerformed(evt);
            }
        });

        btnaddcustomer.setText("Add");
        btnaddcustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaddcustomerActionPerformed(evt);
            }
        });

        btnviewcustomer.setText("View");
        btnviewcustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnviewcustomerActionPerformed(evt);
            }
        });

        btndeletecustomer.setText("Delete");
        btndeletecustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndeletecustomerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(262, 262, 262))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(btnbackcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)
                        .addComponent(btnaddcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnviewcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(btndeletecustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(123, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnbackcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnaddcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnviewcustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btndeletecustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(379, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnbackcustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbackcustomerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbackcustomerActionPerformed

    private void btndeletecustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndeletecustomerActionPerformed

        int selectedrow =jtablemanagecustomers.getSelectedRow();
        if (selectedrow >=0)

        {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null,"Would you like to delete this customer?", "Warning", dialogButton);
            if(dialogResult == JOptionPane.YES_OPTION){
                Customer customer= (Customer) jtablemanagecustomers.getValueAt(selectedrow,0);
               business.getCustomerDirectory().removeCustomer(customer);
                populateCustomerDetails();
            }

        }
        else
        {
            JOptionPane.showMessageDialog(null,"Please Select any row","Warning",JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btndeletecustomerActionPerformed

    private void btnaddcustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaddcustomerActionPerformed

        AddCustomerJPanel addcustomer = new AddCustomerJPanel(userProcessContainer, business , ua);
        userProcessContainer.add("AddCustomerJPanel", addcustomer);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_btnaddcustomerActionPerformed

    private void btnviewcustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnviewcustomerActionPerformed

        ViewCustomerJPanel viewcustomer = new ViewCustomerJPanel(userProcessContainer, business , ua);
        userProcessContainer.add("ViewCustomerJPanel", viewcustomer);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_btnviewcustomerActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaddcustomer;
    private javax.swing.JButton btnbackcustomer;
    private javax.swing.JButton btndeletecustomer;
    private javax.swing.JButton btnviewcustomer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtablemanagecustomers;
    // End of variables declaration//GEN-END:variables
}
