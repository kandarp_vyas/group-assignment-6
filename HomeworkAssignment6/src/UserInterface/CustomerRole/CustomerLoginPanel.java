/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerRole;

import Business.Business;
import Business.Customer.Customer;
import Business.SystemAdministration.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author kanda
 */
public class CustomerLoginPanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
     
    private Business business;
     
    private UserAccount ua; 

    /**
     * Creates new form CustomerLoginPanel
     */
    public CustomerLoginPanel(JPanel userProcessContainer , Business business , UserAccount ua) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.ua = ua;
        cbxCustomerName.removeAllItems();
        
        for (Customer  obj : business.getCustomerDirectory().getCustomerList()) {
            cbxCustomerName.addItem(obj);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNumberOfCustomerPage = new javax.swing.JLabel();
        cbxCustomerName = new javax.swing.JComboBox();
        lblCustomername = new javax.swing.JLabel();
        btnGo = new javax.swing.JButton();

        lblNumberOfCustomerPage.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        lblNumberOfCustomerPage.setText("Number Of Customer(s) Page");

        cbxCustomerName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxCustomerNameActionPerformed(evt);
            }
        });

        lblCustomername.setText("Customer Name:");

        btnGo.setText("Go");
        btnGo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(387, 387, 387)
                        .addComponent(lblNumberOfCustomerPage))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCustomername)
                                .addGap(49, 49, 49)
                                .addComponent(cbxCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(204, 204, 204)
                                .addComponent(btnGo)))))
                .addContainerGap(483, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(lblNumberOfCustomerPage)
                .addGap(74, 74, 74)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCustomername)
                    .addComponent(cbxCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addComponent(btnGo)
                .addContainerGap(518, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoActionPerformed
        // TODO add your handling code here:
        //ManageCustomerShoppingCart obj = new ManageCustomerShoppingCart(userProcessContainer, business, ua)
    }//GEN-LAST:event_btnGoActionPerformed

    private void cbxCustomerNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxCustomerNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxCustomerNameActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGo;
    private javax.swing.JComboBox cbxCustomerName;
    private javax.swing.JLabel lblCustomername;
    private javax.swing.JLabel lblNumberOfCustomerPage;
    // End of variables declaration//GEN-END:variables
}
